require 'capybara'
require 'capybara/dsl'
include Capybara::DSL

module Helpers
  module Auth
    def self.login email="geliyahu@gmail.com", password="12345678"
      visit "lp/login-page#login"
      find("#email").set(email)
      find("#password").set(password)
      click_on "LOGIN"
      has_button?("settings")
    end
  end
  
  def self.close_tooltips
  	sleep 3
  	tooltip_xpath = "//button[contains(@class,'tooltip__close')]"
    find(:xpath, tooltip_xpath).click while (all(:xpath,tooltip_xpath).any?)
  end
end
