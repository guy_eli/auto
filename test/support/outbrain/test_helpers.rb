require 'capybara'
require 'capybara/dsl'
include Capybara::DSL

module OutBrain
  module Helpers
    module Auth
      def self.login email="test123@yopmail.com", password="aA123123"
        visit "/"
        find("#signin-member-username").set(email)
        find("#signin-member-password").set(password)
        find("#loginButton").click
        has_link?("Logout")
      end
    end
  end
end