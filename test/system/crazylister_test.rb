require "application_system_test_case"
# require 'capybara/minitest/assertions'
# include Capybara::Minitest::Assertions

class DahboardTest < ApplicationSystemTestCase
  before do 
    Capybara.app_host = "https://crazylister.com"
  end

  describe "authentication page" do
    it("success login")   { assert Helpers::Auth::login }
    it("incorrect credentials")  do
      assert !Helpers::Auth::login("geliyahu@hotmail.com", "123456") 
      assert has_text?("Sorry, the email or password isn't right. Please try again.")
    end
  end
  describe "new template" do
    before do 
      Helpers::Auth::login
    end
    
    it "Basic category has 7 templates" do 
      click_on "Create New Template"
      click_on "Basic"
      assert has_text?("Anything and Everything")
      assert has_text?("Make it Your Own")
      assert has_text?("Blank")
      assert has_text?("Desk Supplies Galore")
      assert has_text?("Black With White Text")
      assert has_text?("Back to Black")
      assert has_text?("Back to Basics")
    end

    it "'Underwater' is available for edit" do
      visit "/lister?backTo=%2Fapplication%2Fuser%2Fview-templates&id=278718"
      Helpers::close_tooltips
      page.driver.browser.action.send_keys(:enter).perform

      click_on "Text"
      find(:xpath, "//span[contains(text(),'Headline')]").click
      click_on "Edit Headline"

      headline_text = SecureRandom.urlsafe_base64
      template_name = SecureRandom.urlsafe_base64
      page.driver.browser.action.send_keys(headline_text).perform
      click_on "Save"
      sleep 2
      page.driver.browser.action.send_keys(template_name, :enter).perform
      sleep 5
      visit "/application/user/view-templates"
      assert has_text?(template_name)
      click_on template_name
      Helpers::close_tooltips
      assert has_text?(headline_text)
    end
  end
end