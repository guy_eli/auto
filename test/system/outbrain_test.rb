require "application_system_test_case"
# require 'capybara/minitest/assertions'
# include Capybara::Minitest::Assertions

class DahboardTest < ApplicationSystemTestCase
  before do 
    Capybara.app_host = "https://my.outbrain.com/"
  end

  describe "authentication page" do    
    describe "success login" do
      let(:email){"test123@yopmail.com"}
      let(:password){"aA123123"}
      let (:login_result){OutBrain::Helpers::Auth::login(email, password)}
      
      it("for inactive account") do 
        assert login_result
        assert has_text?("Please activate your account!")
      end
    end

    describe "failure login" do
      it("User can't log in with proprietary login and bad password")  do
        assert !OutBrain::Helpers::Auth::login("test123@yopmail.com", "wrong_password") 
        assert has_text?("wrong email or password")
      end

      it("User can't log in with unregistered login")  do
        assert !OutBrain::Helpers::Auth::login("unregistered_user@yopmail.com", "password") 
        assert has_text?("wrong email or password")
      end
      
      describe "blank field" do
        it("password")  do
          assert !OutBrain::Helpers::Auth::login("", "password")
          assert has_text?("wrong email or password")
        end
        it("email")  do
          assert !OutBrain::Helpers::Auth::login("test123@yopmail.com", "") 
          assert has_text?("wrong email or password")
        end
      end
    end
  end
end