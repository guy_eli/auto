require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
Dir[Rails.root.join("test/support/**/*.rb")].each { |f| require f }
require 'minitest/spec'
require 'minitest/autorun'

Capybara.run_server = false

class ActiveSupport::TestCase
  # Add more helper methods to be used by all tests here...
  extend MiniTest::Spec::DSL
  include Capybara::DSL
end
